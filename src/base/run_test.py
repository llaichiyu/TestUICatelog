#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/19 16:15
# @File     : run_test.py
# @Project  : ForTestUICatalog


import unittest, os
from appium.options.ios import *
from appium import webdriver
from time import sleep
from appium.webdriver.common.appiumby import AppiumBy


class BaseAppium(unittest.TestCase):
    def setUp(self):
        capabilities = {
            "platformName": "iOS",
            "appium:automationName": "XCUITest",
            "appium:deviceName": "iPhone 15",
            "appium:bundleId": "com.example.a1p2ple-samplecode.UICatalog",
            "appium:udid": "4E344408-C43B-4D0C-AE8A-B5972E80CFD5"}

        appium_server_url = "http://127.0.0.1:4723/wd/hub"

        self.driver = webdriver.Remote(
            command_executor=appium_server_url,
            options=XCUITestOptions().load_capabilities(caps=capabilities)
        )

    def check_ele_in_page_source(self, ele, page_source):
        if ele in page_source:
            print("element in page source")
        else:
            print("element not in page source")

    def assert_displayed(self, ele):
        assert (ele.is_displayed())

    def get_page_source(self):
        """获取页面page source"""
        page_source = self.driver.page_source
        return page_source

    def find_actions_sheets_in_success(self, value):
        """在点击一个元素成功后跳转到另一个页面时
           检查另一个页面中的元素被find
        """
        ele = self.driver.find_element(by=AppiumBy.IOS_PREDICATE,
                                       value=value)  # 获取成功后的元素
        return ele

    def swipe_up(self, t=500, n=1):
        """向上滑动坐标"""
        window_size = self.driver.get_window_size()
        x1 = window_size['width'] * 0.5  # 起始x
        y1 = window_size['height'] * 0.75  # 起始y
        y2 = window_size['height'] * 0.25  # 终点y
        for i in range(n):
            self.driver.swipe(x1, y1, x1, y2, t)

    def swipe_down(self, t=500, n=1):
        """向下滑动坐标"""
        window_size = self.driver.get_window_size()
        x1 = window_size['width'] * 0.5
        y1 = window_size['height'] * 0.25
        y2 = window_size['height'] * 0.75
        for i in range(n):
            self.driver.swipe(x1, y1, x1, y2, t)

    def swipe_left(self, t=500, n=1):
        """向左滑动坐标"""
        window_size = self.driver.get_window_size()
        x1 = window_size['width'] * 0.75
        y1 = window_size['height'] * 0.5
        x2 = window_size['width'] * 0.25
        for i in range(n):
            self.driver.swipe(x1, y1, x2, y1, t)

    def swipe_right(self, t=500, n=1):
        """向右滑动坐标"""
        window_size = self.driver.get_window_size()
        x1 = window_size['width'] * 0.25
        y1 = window_size['height'] * 0.5
        x2 = window_size['width'] * 0.75
        for i in range(n):
            self.driver.swipe(x1, y1, x2, y1, t)

    def tearDown(self):
        sleep(2)
        if self.driver:
            self.driver.quit()

    print("teardown结束了,driver成功继承于基类")

# if __name__ == '__main__':
#     a.setup()
