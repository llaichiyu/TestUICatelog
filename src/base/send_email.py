#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/19 16:18
# @File     : send_email.py
# @Project  : ForTestUICatalog


import smtplib
from email.mime.text import MIMEText
from email.header import Header
from email.mime.multipart import MIMEMultipart
import os

from src.utils.get_some_data import get_smtpserver, get_mail_user, get_mail_pass, get_sender, get_receiver, get_subject, \
    get_local_host, get_smtp_port


class SendEmail():
    def __init__(self, subject, sender, receiver, smtp_server, smtp_port, localhost, mail_user, mail_pass,
                 latest_report):
        """init params"""
        self.subject = subject
        self.sender = sender
        self.receiver = receiver
        self.smtpserver = smtp_server
        self.smtpport = smtp_port
        self.localhost = localhost
        self.mailuser = mail_user
        self.mailpass = mail_pass
        self.latest_report = latest_report

    def send_email(self, latest_report):
        """rewrite send_email"""
        f = open(file=latest_report, mode='r', buffering=8192, encoding='utf-8')
        mail_content = f.read()
        msg = MIMEText(_text=mail_content, _subtype='html', _charset='utf-8')
        msg['Subject'] = Header(s=self.subject, charset='utf-8')
        msg['From'] = self.sender
        msg['To'] = '.'.join(self.receiver)
        smtp = smtplib.SMTP_SSL(host=self.smtpserver, port=self.smtpport, local_hostname=self.localhost)
        smtp.helo(name=self.smtpserver)
        smtp.ehlo(name=self.smtpserver)
        smtp.login(user=self.mailuser, password=self.mailpass)
        print("sending~")
        smtp.sendmail(from_addr=self.sender, to_addrs=self.sender, msg=msg.as_string())
        smtp.quit()
        print("FINISH YET~!")
        f.close()

    # if __name__ == '__main__':
    #     dir = './src/report/'
    #     get_latest_report(dir)
