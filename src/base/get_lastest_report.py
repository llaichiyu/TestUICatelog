#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/19 16:18
# @File     : get_lastest_report.py
# @Project  : ForTestUICatalog


import os


def get_latest_report(report_dir):
    """for get lastest report"""
    for root, dirs, files in os.walk(report_dir):
        __a__ = files
        __b__ = __a__[-1]
        report = os.path.join(report_dir, __b__)
        """操作files"""
        return report


import os
import glob


def get_latest_file(directory):
    list_of_files = glob.glob(directory + '/*')  # 获取目录下所有文件
    print("目录下的所有文件： ", list_of_files)
    latest_file = max(list_of_files, key=os.path.getctime)  # 获取最新的文件
    return latest_file


if __name__ == '__main__':
    # 指定目录路径
    directory_path = '/Users/stackdin/code/ForTestUICatalog/src'

    latest_file = get_latest_file(directory_path)
    print("最新的文件是：", latest_file)
