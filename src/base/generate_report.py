#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/19 16:18
# @File     : generate_report.py
# @Project  : ForTestUICatalog


from src.test_report.test_report import HTMLTestReportEN
from src.utils.get_some_data import get_now
from src.utils.get_some_data import get_start_dir, get_top, get_tester, get_title, get_description, get_pattern


def run_case_and_generate_report():
    import unittest
    filename = get_start_dir() + get_now() + 'report.html'
    with open(filename, 'wb') as report:
        discover = unittest.defaultTestLoader.discover(start_dir=get_start_dir(), pattern=get_pattern(),
                                                       top_level_dir=get_top())  # use discover and get all cases in discover

        runner = HTMLTestReportEN(stream=report, verbosity=2, title=get_title(), description=get_description(),
                                  tester=get_tester())
        runner.run(discover)
        report.close()
        return report
