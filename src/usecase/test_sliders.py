#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/21 14:22
# @File     : test_sliders.py
# @Project  : ForTestUICatalog

from src.base.run_test import BaseAppium, AppiumBy


class TestSlider(BaseAppium):
    def test_slider(self):
        """
        这是slider
        :return:
        """
        self.swipe_up()
        # self.swipe_down()
        # self.swipe_left()
        # self.swipe_right()
