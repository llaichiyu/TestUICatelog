#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/20 16:28
# @File     : test_activity_indicators.py
# @Project  : ForTestUICatalog


from src.base.run_test import BaseAppium, AppiumBy


class TestActivityIndicators(BaseAppium):
    def test_activity_indicators(self):
        ele_activity_indicators = self.driver.find_element(by=AppiumBy.XPATH,
                                                           value='//XCUIElementTypeStaticText[@name="Activity Indicators"]')
        ele_activity_indicators.click()
        ele_gray = self.driver.find_element(by=AppiumBy.IOS_CLASS_CHAIN,
                                            value='**/XCUIElementTypeOther[`name == "GRAY"`][1]')
        ele_gray1 = self.driver.find_element(by=AppiumBy.XPATH, value='(//XCUIElementTypeOther[@name="GRAY"])[1]')
