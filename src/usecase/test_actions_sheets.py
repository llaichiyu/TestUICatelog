#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/19 16:23
# @File     : test_actions_sheets.py
# @Project  : ForTestUICatalog

from src.base.run_test import BaseAppium, AppiumBy


class TestDrivvv(BaseAppium):
    def test_find_action_sheets(self):
        """找到第一个元素"""
        ele_actions_sheets = self.driver.find_element(by=AppiumBy.IOS_PREDICATE, value='name == "Action Sheets"')
        page_source = self.get_page_source()
        self.check_ele_in_page_source(ele_actions_sheets.text,
                                      page_source)  # 检查一个对象是否在page source中，要传入两个参数，一个是元素.text，另一个是page source。不能直接使用元素是否在page source中。
        ele_actions_sheets.click()

    def test_detail_actions_sheets(self):
        ele_actions_sheets = self.driver.find_element(by=AppiumBy.IOS_PREDICATE,
                                                      value='name == "Action Sheets"')
        ele_actions_sheets.click()  # 找到第一个元素并点击
        ele_ano_1 = self.find_actions_sheets_in_success(value='name == "Okay / Cancel"')
        ele_ano_2 = self.find_actions_sheets_in_success(value='name == "Other"')
        page_source = self.get_page_source()
        self.check_ele_in_page_source(ele_ano_1.text, page_source)
        self.check_ele_in_page_source(ele_ano_2.text, page_source)
        ele_ano_1.click()
        okay_ele = self.driver.find_element(by=AppiumBy.IOS_PREDICATE, value='name == "OK"')
        print("这是ok_button", okay_ele)
        okay_ele.click()
        self.driver.implicitly_wait(5)
        print("已经结束了~~~")

    # self.check_ele_in_page_source(ok_button.text, page_source)


def get_current_ele(self):
    ele = self.driver.find_element(by=AppiumBy.IOS_PREDICATE, value='name == "Action Sheets"')  # 获取当前元素
    return ele
