#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/21 09:49
# @File     : test_alert_views.py
# @Project  : ForTestUICatalog
from src.base.run_test import BaseAppium, AppiumBy


class TestAlertViews(BaseAppium):
    def test_alert_views_exits(self):
        """
        1.判断alert views元素是否存在
        2.点击alert
        3.判断simple元素是否存在
        4.点击simple
        5.判断ok元素是否存在
        6.点击ok
        7.点击ok后，跳转到的页面的page source中是否包含Alert Views
        """
        # ele_alert_views = self.driver.find_element(by=AppiumBy.IOS_PREDICATE, value='name == "Alert Views"')
        # ele_alert_views.click()
        #
        # ele_simple = self.driver.find_element(by=AppiumBy.IOS_PREDICATE, value='name == "Simple"')
        # ele_simple.click()
        #
        # ele_ok = self.driver.find_element(by=AppiumBy.XPATH, value='//XCUIElementTypeButton[@name="OK"]')
        # ele_ok.click()
        # self.driver.implicitly_wait(100)
        #
        # self.check_ele_in_page_source(self.get_uicatalog().text, self.get_page_source())
        # self.get_uicatalog().click()  # 点击uicatelog
        #
        # self.get_text_entry()  # 测试text_entry能不能正常运行
        # print("get_text_entry()运行结束了")
        self.get_text_entry()

    def get_text_entry(self):
        """
        要能成功获取text entry并点击text entry
        :return:
        """
        ele_alert_views = self.driver.find_element(by=AppiumBy.IOS_PREDICATE, value='name == "Alert Views"')
        ele_alert_views.click()  # 进入到alert_views页面

        ele_text_entry = self.driver.find_element(by=AppiumBy.IOS_PREDICATE, value='name == "Text Entry"')
        ele_text_entry.click()  # 点击ele_text_entry

        ele_input = self.driver.find_element(by=AppiumBy.IOS_PREDICATE, value='type == "XCUIElementTypeTextField"')
        ele_input.send_keys('我去你大爷的去死凸(艹皿艹 )')

        ele_ok = self.driver.find_element(by=AppiumBy.XPATH, value='//XCUIElementTypeButton[@name="OK"]')
        ele_ok.click()  # 点击ok

    def get_uicatalog(self):
        ele_uicatalog = self.driver.find_element(by=AppiumBy.IOS_PREDICATE,
                                                 value='name == "UICatalog" AND label == "UICatalog" AND type == "XCUIElementTypeButton"')
        return ele_uicatalog
