#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/21 16:02
# @File     : test_s_down.py
# @Project  : ForTestUICatalog

from src.base.run_test import BaseAppium, AppiumBy


class T(BaseAppium):
    def test_a1(self):
        self.swipe_down()
