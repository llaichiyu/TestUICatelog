#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/19 16:23
# @File     : get_some_data.py.py
# @Project  : ForTestUICatalog


def get_wrong_x_site():
    return '99999991911191911919919zzzzz'


def get_content():
    return 'application/json'


def get_username():
    return ''


def get_userId():
    return '1045'


def get_stoCode():
    return '10000'


def get_client():
    return '10000003'


import time


def get_now():
    return time.strftime('%Y-%m-%d_%H_%M_%S_')


def get_smtpserver():
    return 'smtp.qq.com'


def get_mail_user():
    return '302869907@qq.com'


def get_mail_pass():
    return 'wxmkxqbkdqlocbae'


def get_sender():
    return '302869907@qq.com'


def get_receiver():
    return ['302869907@qq.com']


def get_subject():
    return '接口测试结果--by liyalan'


def get_local_host():
    return '127.0.0.1'


def get_smtp_port():
    return '465'


def get_start_dir():
    return './src/usecase'


def get_top():
    return './'


def get_tester():
    return 'pauline'


def get_title():
    return 'Test Result'


def get_description():
    return '用例执行情况,带截图，带饼图，带详情'


def get_pattern():
    return 'test*.py'


def get_db_host():
    return ''


def get_db_port():
    return 3306


def get_db_user():
    return 'root'


def get_db_pass():
    return ''


def get_db_name():
    return ''


def get_db_charset():
    return 'utf8'
