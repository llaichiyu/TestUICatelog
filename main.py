#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author   : paulinelee
# @Time     : 2024/3/19 16:20
# @File     : main.py
# @Project  : ForTestUICatalog


from src.base.send_email import SendEmail
from src.base.generate_report import run_case_and_generate_report
from src.utils.get_some_data import get_smtpserver, get_mail_user, get_mail_pass, get_sender, get_receiver, get_subject, \
    get_local_host, get_smtp_port
from src.base.get_lastest_report import get_latest_file, get_latest_report

if __name__ == "__main__":
    run_case_and_generate_report()
    report_dir = '/Users/stackdin/code/ForTestUICatalog/src'
    latest_report = get_latest_report(report_dir=report_dir)

    # latest_report = get_latest_file(directory=report_dir)
    send_email = SendEmail(subject=get_subject(), sender=get_sender(), receiver=get_receiver(),
                           smtp_server=get_smtpserver(), smtp_port=get_smtp_port(), localhost=get_local_host(),
                           mail_user=get_mail_user(), mail_pass=get_mail_pass(), latest_report=latest_report)
    send_email.send_email(latest_report)
